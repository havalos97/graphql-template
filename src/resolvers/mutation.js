import UserModel from '../models/UserModel.js'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken';
const SECRET_JWT_KEY = process.env.SECRET_KEY;

const Mutation = {
	signup: async (parent, {input}, {pubsub}) => {
		const {email, name, lastName, password} = input

		const user = await UserModel.findOne({email});
		if (user) {
			throw new Error('Email already exists')
		}

		// Devolvemos el password protegido
		const hashedPassword = await bcrypt.hash(password, 10)
		const newUser = new UserModel({
			name,
			lastName,
			email,
			password: hashedPassword
		})

		newUser.save();
		pubsub.publish('user-added', {
			newUser
		});

		const token = jwt.sign({
			userID: newUser.id,
		}, SECRET_JWT_KEY);
		return {
			user: newUser,
			token,
		}
	},
	login: async(parent, {email, password}) => {
		// Find user to verify if it exists
		const user = await UserModel.findOne({email}).exec();
		if (!user) {
			throw new Error('Unauthorized!');
		}
		// Verify that password is valid
		const isValid = await bcrypt.compare(password, user.password);
		if (!isValid) {
			throw new Error('Unauthorized!');
		}
		// If all is ok, generate a new token
		const token = jwt.sign({userID:user._id}, SECRET_JWT_KEY);
		return {
			user,
			token,
		}
	},
	addTour: () => {
		// agregar operaciones
	}
}

export default Mutation