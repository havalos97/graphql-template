const Subscription = {
	newUser: {
		subscribe: (parent, args, { pubsub }) => {
			return pubsub.asyncIterator(['user-added']);
		}
	}
};

export default Subscription;