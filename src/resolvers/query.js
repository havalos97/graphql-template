import UserModel from '../models/UserModel'

const Query = {
	status: () => 'Hola desde GraphQL',
	getUsers: (parent, args, context) => {
		const { currentUser } = context;
		if (!currentUser) {
			throw new Error('Unauthorized!');
		}
		return UserModel.find().exec()
	},
	getTours: () => {
		// return ToursModel.find().exec()
	}
}

export default Query