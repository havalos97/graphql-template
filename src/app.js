import 'dotenv/config';
import {createServer} from 'http';
import express from 'express'
import ExpressPlayground from 'graphql-playground-middleware-express';
import {ApolloServer, PubSub} from 'apollo-server-express'
import mongoose from 'mongoose'
import schema from './schema'
import resolvers from './resolvers'
import jwt from 'jsonwebtoken';
const SECRET_JWT_KEY = process.env.SECRET_KEY;

const app = express()
const port = process.env.APP_PORT

const pubsub = new PubSub();
const server = new ApolloServer({
	typeDefs: schema,
	resolvers,
	context: (props) => {
		const request = props.req;
		const connection = props.connection;

		let currentUser;
		let token;
		if (request) {
			token = request.headers.authorization;
		} else {
			token = connection.context.Authorization;
		}
		currentUser = jwt.verify(token, SECRET_JWT_KEY);

		return {
			...request,
			currentUser,
			pubsub,
		}
	},
});

const httpServer = createServer(app);
server.installSubscriptionHandlers(httpServer);
server.applyMiddleware({app})

app.get('/', (request, response) => {
	const token = request.headers.authorization;
	if (token) {
		if (jwt.verify(token, SECRET_JWT_KEY)) {
			return response.send('Welcome to the API');
		}
		return response.send('Unauthorized');
	}
	return response.send('Unauthorized');
})
app.get('/playground',
	ExpressPlayground({
		endpoint: '/graphql'
	})
)

mongoose.connect(
	'mongodb+srv://bedu:PY31hcV7bYTenmKB@cluster0-9t6s4.mongodb.net/bedu_mongo?retryWrites=true&w=majority',
	{useNewUrlParser: true, useUnifiedTopology: true}
).then(() => {
	console.log('Connected to MongoDB')
	httpServer.listen(port, () => {
		console.log(`Server running on http://localhost:${port}`)
	})
}).catch(err => {
	console.log(err)
	throw err
})
