import mongoose, {Schema} from 'mongoose'

// Esto ayuda a consumir mongoDB más fácil
const UserSchema = new Schema({
  name: String,
  lastName: String,
  email: String,
  dateBirth: String,
  password: String
}, {
  timestamps: true
})

const UserModel = mongoose.model('User', UserSchema)

export default UserModel