import SchemaDefinitions from './schemaDefinitions.graphql'
import Query from './query.graphql'
import Mutation from './mutation.graphql'
import Subscription from './subscription.graphql'
import Types from './types.graphql'

export default [
  SchemaDefinitions,
  Types,
  Query,
  Mutation,
  Subscription,
]